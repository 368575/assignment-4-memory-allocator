#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 8000

void test_malloc()
{
    puts("Test 1. Simple memory allocation.\n");
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "heap_init() failed");
    puts("Initial heap status:");
    debug_heap(stdout, heap);

    void* ptr = _malloc(25);
    assert(ptr && "_malloc() failed");
    puts("\nHeap after malloc:");
    debug_heap(stdout, heap);

    _free(ptr);
    heap_term();
}

void test_free()
{
    puts("\n\nTest 2. Freeing one block from multiple allocations.\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "heap_init() failed");
    puts("Initial heap status:");
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(25);
    assert(ptr1 && "_malloc() failed");

    void* ptr2 = _malloc(125);
    assert(ptr2 && "_malloc() failed");

    void* ptr3 = _malloc(40);
    assert(ptr3 && "_malloc() failed");

    puts("\nHeap after allocation of 3 blocks:");
    debug_heap(stdout, heap);

    _free(ptr2);
    puts("\nHeap after free second block:");
    debug_heap(stdout, heap);

    _free(ptr1);
    _free(ptr3);
    heap_term();
}

void test_free_2()
{
    puts("\n\nTest 3. Freeing two blocks from multiple allocations.\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "heap_init() failed");
    puts("Initial heap status:");
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(100);
    assert(ptr1 && "_malloc() failed");

    void* ptr2 = _malloc(24);
    assert(ptr2 && "_malloc() failed");

    void* ptr3 = _malloc(48);
    assert(ptr3 && "_malloc() failed");

    puts("\nHeap after allocation of 3 blocks:");
    debug_heap(stdout, heap);

    _free(ptr3);
    _free(ptr2);
    puts("\nHeap after free 2 blocks:");
    debug_heap(stdout, heap);

    _free(ptr1);
    heap_term();
}

void test_heap_grow_extend()
{
    puts("\n\nTest 4. Extending heap after the memory has run out.\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "heap_init() failed");
    puts("Initial heap status:");
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(8000);
    assert(ptr1 && "_malloc() failed");

    puts("\nHeap after the memory has almost run out:");
    debug_heap(stdout, heap);

    void* ptr2 = _malloc(HEAP_SIZE);
    assert(ptr2 && "_malloc() failed");

    puts("\nHeap extended after new huge allocation:");
    debug_heap(stdout, heap);

    _free(ptr2);
    _free(ptr1);
    heap_term();
}

void test_heap_grow_no_extend()
{
    puts("\n\nTest 5. Heap cannot be expanded, new region in different location\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "heap_init() failed");
    puts("Initial heap status:");
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(8000);
    assert(ptr1 && "_malloc() failed");

    puts("\nHeap after the memory has almost run out:");
    debug_heap(stdout, heap);

    void* reserved = mmap(HEAP_START + REGION_MIN_SIZE, 1, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    void* ptr2 = _malloc(1000);
    assert(ptr2 && "_malloc() failed");

    puts("\nHeap after growth:");
    debug_heap(stdout, heap);

    munmap(reserved, 1);
    _free(ptr2);
    _free(ptr1);
    heap_term();
}

int main()
{
    test_malloc();
    test_free();
    test_free_2();
    test_heap_grow_extend();
    test_heap_grow_no_extend();
    return 0;
}
